from datetime import date
from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import math
import time

options = webdriver.ChromeOptions()
options.add_argument("--disable-gpu")
options.add_argument("--headless")
driver = webdriver.Chrome(options=options)

url = 'https://dom.trojmiasto.pl/nieruchomosci-rynek-wtorny/ai,_500000,e1i,2,wi,100.html'
driver.get(url)
number = driver.find_element_by_xpath('//div[@class="ogloszeniaCounter"]/p/strong')

titles = []
locations = []
prices = []
sizes = []
links = []
pricesPerMeter = []
fileName = str(date.today()) + ' mieszkania.csv'

pages = math.ceil(int(number.text) / 60)
for page in range(0, pages):
    content = driver.page_source
    soup = BeautifulSoup(content, 'html.parser')
    for a in soup.findAll('div', attrs={'class': 'ogloszeniaList__item'}):
        title = a.find('a', attrs={'class': 'ogloszeniaList__title'})
        location = a.find('p', attrs={'class': 'ogloszeniaList__location'})
        price = a.find('p', attrs={'class': 'ogloszeniaList__price'})
        size = a.find('div', attrs={'class': ['ogloszeniaList__detail', 'button', 'button--fourth', 'button--label']})
        link = a.find('a', attrs={'class': 'ogloszeniaList__img'})
        p = price.text.split('\xa0')[0].replace(' ', '')
        m = size.text.split('\xa0')[0].replace(' ', '')
        ppm = round((float(p) / float(m)), 2)
        titles.append(title.text)
        locations.append(location.text)
        prices.append(price.text)
        sizes.append(size.text)
        links.append(link.get('href'))
        pricesPerMeter.append(ppm)
    if page < pages - 1:
        driver.get(url + '?strona=' + str(page + 1))
        time.sleep(5)
# Create data structure with columns and assign variables to it
# TODO Set data types for these objects when migrating to database
df = pd.DataFrame(
    {'Opis': titles, 'Dzielnica': locations, 'Cena': prices, 'Wielkość': sizes, 'Cena za M2': pricesPerMeter,
     'Link': links}).sort_values(by='Cena za M2', ascending=False)

# Save created data structur to CSV file
# TODO Check if file doesn't exist, else do loop to see if
df.to_csv('prices/'+fileName, index=False, encoding='utf-8')

driver.quit()
